/*
Question 2
There are 2 tables - a customer table with one record per customer, 
and a task table with one record per task. 

The cardinality of the relationship between the two tables is 1-n as a customer can have many tasks.

On the tasks table there is a task id, the timestamp of the task, and a task category, 
e.g. Cleaning, Handyman, Removals, etc.
The question we'd like answered is - ​what category did each customer create their first task in?

Prepare some SQL to generate a resultset that has the customers name 
(found on the customer table), and the category of the customers first task ever on the platform.
  Define and use additional tables if you require them.
*/

--Continue to use schema created in Q1
set search_path to airtasker;

create extension if not exists "uuid-ossp" with schema airtasker;

drop table if exists customer;

create table if not exists customer (
    id              uuid,
    first_name      text,
    middle_name     text,
    last_name       text
);

--pk
ALTER TABLE customer ADD PRIMARY KEY (id);
ALTER TABLE task     ADD PRIMARY KEY (id);
--unique
ALTER TABLE customer ADD CONSTRAINT uq_customer_id UNIQUE (id);
ALTER TABLE task     ADD CONSTRAINT uq_task_id UNIQUE (id);
--extent to have category
alter table task add column category text;
--add fk field
alter table task add column customer_id uuid;
--fk
ALTER TABLE task add CONSTRAINT fk_customer_id 
FOREIGN KEY (customer_id) REFERENCES customer(id);

--Populate customer data
insert into customer (id, first_name, last_name) 
select
   uuid_generate_v4()::uuid,
   md5(random()::text)::text as first_name,
   md5(random()::text)::text as last_name
from generate_series(1, 150);


--Category table
create table category (
  id   serial,
  name text
);

insert into category (name) values
 ('Cleaning'),
 ('Handyman'),
 ('Removals'),
 ('Window Cleaning'),
 ('Gutter Cleaning'),
 ('Car Washing'),
 ('Pool Cleaning'),
 ('Airbnb Cleaning'),
 ('Apartment Cleaning'),
 ('Attic Cleaning'),
 ('Balcony Cleaning'),
 ('BBQ Cleaning'),
 ('Bed Cleaning'),
 ('Blind Cleaning'),
 ('Brick Cleaning'),
 ('Carpet Cleaning'),
 ('Ceiling Cleaning'),
 ('Commercial Cleaning'),
 ('Concrete Floor Cleaning'),
 ('Couch Cleaning'),
 ('Curtain Cleaning');

select * from category;

--add customer to task
--add category to task

--2k task
select count(*) from task;

--randomly pick 1 customer id
drop function if exists random_cid();
CREATE or replace FUNCTION random_cid() RETURNS uuid AS
$$ SELECT id from customer ORDER BY RANDOM() LIMIT 1 $$ LANGUAGE SQL;

--randomly pick 1 category id
drop function if exists random_cat();
CREATE or replace FUNCTION random_cat() RETURNS text AS
$$ SELECT name from category ORDER BY RANDOM() LIMIT 1 $$ LANGUAGE SQL;

--Randomly assign customer and category to task
update task
set customer_id = random_cid(),
    category    = random_cat()
;

--Preview
select * from task;


--Q2: Category of Each users' earilest post
select 
  c.first_name,
  c.last_name,
  task.category
from customer c
join 
(
  select
    min(post_date) as early_date,
    customer_id    as c_id
  from task
  group by
    customer_id
) t on t.c_id = c.id
join task 
  on task.post_date = t.early_date and task.customer_id = t.c_id  
  --TODO: index in production 
order by 1
;


--Test case for 7 users:
/*
04aeba6f-7e22-459a-b193-c0e11b475680	7b29647c33e7bfe4444598bb66554a99	72de8febe6949118d6ce924b8133614f	Bed Cleaning
07614dba-2e49-4cf5-8510-24d9bac60b46	03006de979ceb1e6813ef6f83672c238	df8a38faabdc034c7f7af338682bbc79	Carpet Cleaning
09617626-1488-46e4-8301-e2a00fd79d87	634e13c5944a4428c9bf89d53fb444bf	46095983a73f140d5ee24e8ff3f22621	Balcony Cleaning
0a5d6d62-2cf6-4b3a-9e80-7de02bf63670	912ee9383ed13e7455e3b5b547d12dcb	893e3b15b23a521c28531ebe50fd2c6e	Removals
0b3af3da-b568-46c6-8998-b97eeeb08dff	f0ed792c71fd1d9ce00547c426ac308f	144c73d26b26731d502e5a682fb14b7e	Ceiling Cleaning
0cb68db0-34d4-4a36-901d-bbade70afa0e	6fa8dc56809a36806d92eba30b2b65a2	8b49927f1e95b3c03226b98cb193c9e2	Gutter Cleaning
0dc24b56-9a88-4e2e-ba6e-1baa3749e376	4b7ff4a735dda887c569f411b0a5b2ef	1edec409c000f90bbb5dacd819285e72	Removals
*/

--might use dense_rank() if performance is poor
select min(post_date), t.category
from task t 
where customer_id in (
  '04aeba6f-7e22-459a-b193-c0e11b475680'    --Hash may change
--   '07614dba-2e49-4cf5-8510-24d9bac60b46'
--   '09617626-1488-46e4-8301-e2a00fd79d87'
--   '0a5d6d62-2cf6-4b3a-9e80-7de02bf63670'
--   '0b3af3da-b568-46c6-8998-b97eeeb08dff'
--   '0cb68db0-34d4-4a36-901d-bbade70afa0e'
--   '0dc24b56-9a88-4e2e-ba6e-1baa3749e376'
)
group by t.category
order by 1 asc limit 1;

