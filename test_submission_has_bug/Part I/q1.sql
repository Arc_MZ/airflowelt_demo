/* Question 1
There is a table of tasks, one record per task.

On the table there is a Task Posted Date, 
indicating when the task was first posted to the site, and a Task Closed Date,
indicating when the task was no longer active, being either completed, or cancelled.
The question we'd like answered is 

- ​what is the daily active task count? 

Prepare some SQL to generate a table that has one record per day, 
with a count of the number of active tasks for each day.

Note a task is considered active 
between it's Task Posted Date and it's Task Closed Date.

Define and use additional tables if you require them.
*/

-- drop schema airtasker cascade;
create schema airtasker;

set search_path to airtasker,public;

drop table if exists airtasker.task;

create table task (
  id          serial,
  post_date   date,  --can be timestamp for better granuarity.
  closed_date date
);

--Q: ​what is the daily active task count?

--Create date factory to store series
create table if not exists date_factory as
select
  row_number() over () as id,
  t.*
from (
  select 
  generate_series((date '2017-09-27'), (date '2018-09-27'),interval '1 day') as day
) t;

select * from date_factory;

CREATE EXTENSION "tablefunc"; --normal distribution generator

drop table if exists tmp_task_dates;

--Get day offset, mean=2, variance=3
create table day_offset as
select case when day_offset < 0 then -1*t.day_offset else t.day_offset end
from (SELECT normal_rand(12, 2, 3)::int as day_offset) t;

select * from day_offset;

--Generate tasks
with offset_st as (
  select case when day_offset < 0    --non-negative number
              then -1 * t.day_offset 
         else t.day_offset end
  from (select normal_rand(10, 2, 3)::int as day_offset) t --10 variant
),
random_date as (  
  select 
    df.id,
    df.day::date
  from date_factory df 
  join (
    select
      generate_series (1, 200)     as id, 
      (ceil(random()* 366))::int   as val
  ) t on t.val = df.id
)
select 
  row_number() over () as id, 
  day                  as post_date,
  day + day_offset     as closed_date,
  day_offset
into temp tmp_task_dates    --temp table
from offset_st ost 
cross join random_date rd  --200*10 variant = 2k
order by 2, 3;

insert into task (post_date, closed_date)
select post_date, closed_date from tmp_task_dates;

select * from task;

--Build main table for Q1
create table daily_active_tasks as
select
  days.day :: date,
  count(distinct task.id) as num_active
from task
right join (
  select
  generate_series((date '2017-09-27'), (date '2018-09-27'), interval '1 day') as day
) days on (task.post_date <= days.day and days.day < task.closed_date)     
--inclusive for post_date,but close day strictly after today
group by
  days.day;

select * from daily_active_tasks;



--Test 1 cnt=10
select * from task
where task.post_date <= '2017-09-27'::date and '2017-09-27'::date < task.closed_date;

--Test 2 cnt=2
select * from task
where task.post_date <= '2017-10-31'::date and '2017-10-31'::date < task.closed_date;

--Test 3 cnt=20
select * from task
where task.post_date <= '2017-11-14'::date and '2017-11-14'::date < task.closed_date;



--Validate 
select * from daily_active_tasks where day in ('2017-09-27'::date, '2017-10-31'::date, '2017-11-14'::date);


