with r_cat as (
  select id, name from category order by random()
),
r_cus as (
  select id from customer order by random()
),
content as (
  select 
    r_cus.id     as c_id,
    r_cat.id     as cat_id,
    r_cat.name   as category
  from r_cat
  cross join r_cus
  order by random() limit 2000 --as same as number of task
)
update task
set customer_id = content.c_id,
    category = content.category
from content;


with r_cat as (
    select row_number() over() as r_id,
           t.*
    from ( select id, name from category order by random() ) t
),
r_cus as (
    select row_number() over() as r_id,
           t.*
    from ( select id from customer order by random() ) t
),
content as (
    select row_number() over() as r_id,
           t.*
    from (
      select 
          r_cus.id     as c_id,
          r_cat.id     as cat_id,
          r_cat.name   as category
      from r_cat
      cross join r_cus
      order by random() limit 2000 --as same as number of task
    ) t
)
update task
set customer_id = content.c_id,
    category    = content.category
from content, r_cus, r_cat
where content.r_id = r_cus.r_id and 
      content.r_id = r_cat.r_id;
