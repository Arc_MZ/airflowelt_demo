from airflow.utils.decorators import apply_defaults
from airflow.plugins_manager import AirflowPlugin
from airflow.operators.postgres_operator import PostgresOperator


class AirtaskerELTTask(PostgresOperator):
    """
    Use postgres_airflow as default connection

    Will show up under airflow.operators.airtasker_plugins.AirtaskerELTTask
    """
    @apply_defaults
    def __init__(
            self, sql,
            postgres_conn_id='postgres_airflow',
            database='airflow',
            autocommit=False,
            parameters=None,
            *args, **kwargs):

        super(PostgresOperator, self).__init__(*args, **kwargs)
        self.database = database
        self.sql = sql
        self.postgres_conn_id = postgres_conn_id
        self.autocommit = autocommit
        self.parameters = parameters


# Defining the plugin class
class AirtaskerPlugs(AirflowPlugin):
    name = "airtasker_plugins"
    operators = [
        AirtaskerELTTask
    ]
