# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function
from datetime import timedelta

import airflow
from airflow.models import Variable
# ignore broken link in pycharm ide, instead run in docker compose
from airflow.operators.airtasker_plugins import AirtaskerELTTask


# require rebuild docker image with latest config/airflow.cfg
args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(0),
    'provide_context': True,
    # 'email': ['zhuangdeyouxiang@gmail.com'],
    # 'email_on_failure': True,
    # 'email_on_retry': True,
}

# We don't expose Variable there
SQL_PATH = './sqls'

dag1 = airflow.DAG(
    'load_task_activity_daily_snapshot',
    dagrun_timeout=timedelta(minutes=60),
    template_searchpath=SQL_PATH,
    default_args=args,
    max_active_runs=1
)

stage_task = AirtaskerELTTask(
    task_id='task2a',
    sql='view/task_view.sql',
    dag=dag1)

stage_user = AirtaskerELTTask(
    task_id='task2b',
    sql='view/user_view.sql',
    dag=dag1)

# TODO: write some bad sql to fail this task...
# ELTTask should be wrapped up in a transaction..
snapshot_present_user_and_task = AirtaskerELTTask(
    task_id='task1',
    sql='snapshot/snapshot_user_and_task.sql',
    dag=dag1,
) #autocommit=True

# Group 1:
#   2a, 2b -> 1
stage_task >> snapshot_present_user_and_task
stage_user >> snapshot_present_user_and_task

create_task_activity_view = AirtaskerELTTask(
    task_id='task3',
    sql='view/task_activity_view.sql',
    dag=dag1)

snapshot_task_activity = AirtaskerELTTask(
    task_id='task4',
    sql='snapshot/snapshot_task_activity.sql',
    dag=dag1)

# Group 2:
#   1 -> 3 -> 4
snapshot_present_user_and_task >> create_task_activity_view >> snapshot_task_activity

if __name__ == "__main__":
    dag1.cli()
