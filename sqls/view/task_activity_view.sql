/*
  NOTE: Views are all belongs to Staging schema.
*/
create or replace view staging.task_activity as
select
    t.id                                              as task_id,
    t.name                                            as name,

    now()::date                                       as snapshot_date,

    sum(case when type = 'View'    then 1 else 0 end) as view_count,
    sum(case when type = 'Offer'   then 1 else 0 end) as offer_count,
    sum(case when type = 'Comment' then 1 else 0 end) as comment_count

from sources.activity a 
inner join presentation.tasks t
    on a.task_id = t.id
where
    t.current_records = true  --ignore previous snapshots, focus on current batch.
group by 
    1,2,3
order by
    1,2,3
;
/*
task_id | name                      | snapshot_date       | view_count | offer_count | comment_count
--------+---------------------------+---------------------+------------+-------------+--------------
      1 | clean 1 bedroom house     | 2018-09-30 19:36:20 |          1 |           1 |             2
      2 | clean 4 bedroom house     | 2018-09-30 19:36:20 |          0 |           1 |             0
      3 | clean 3 bedroom house     | 2018-09-30 19:36:20 |          3 |           0 |             0
      4 | mow lawn                  | 2018-09-30 19:36:20 |          0 |           2 |             1
      5 | remove garbage            | 2018-09-30 19:36:20 |          0 |           0 |             2
      6 | clean 1 bedroom apartment | 2018-09-30 19:36:20 |          2 |           1 |             0
      7 | design web page           | 2018-09-30 19:36:20 |          0 |           1 |             2
      8 | costume makeup for party  | 2018-09-30 19:36:20 |          0 |           4 |             3
      9 | post party cleanup        | 2018-09-30 19:36:20 |          1 |           1 |             1
     10 | build tiki bar            | 2018-09-30 19:36:20 |          1 |           1 |             0
     11 | clean 1 bedroom apartment | 2018-09-30 19:36:20 |          1 |           0 |             0
     12 | repair smashed tiger      | 2018-09-30 19:36:20 |          0 |           1 |             2
*/

/*
 Alternatively,

select
    t.id                                              as task_id,
    t."name"                                          as name,
    now()                                             as snapshot_date,
    count(*) filter (where type='View')               as view_count,
    sum(case when type = 'Offer' then 1 else 0 end)   as offer_count,
    sum(case when type = 'Comment' then 1 else 0 end) as comment_count
from sources.activity a
inner join presentation.tasks t on a.task_id = t.id
group by
    1,2,3
order by
    1,2,3
;
 */