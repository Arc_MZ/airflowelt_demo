create or replace view staging.users as
select distinct
    u.id,
    coalesce(u.firstname,'') || ' ' || coalesce(u.surname,'') as user_name,
    -- Assume: Not active if user don't have any activities record.
    -- which means they only registered, but haven't start requesting job or interacting with tasks.
    case when a.type is null then false
         else true
    end as user_active_flag
from sources.user u
left join sources.task t     on t.user_id = u.id
left join sources.activity a on a.task_id = t.id
order by
  u.id;


/** Add 1 user with 0 activity to test

insert into sources.user (id,firstname, surname) values (33,'john', 'nul');

*/