create or replace view staging.tasks as
select 
    id,
    name,
    case state
        when 'active'          then 'Active'
        when 'suspend'         then 'Cancelled by Admin'
        when 'payment pending' then 'Pending'       --cloud have more granular states.
        else                        'Ended'
    end as status
from sources.task;


/* Distinct Task State:

  active
  payment pending

  finalised
  cancelled

  suspend
*/