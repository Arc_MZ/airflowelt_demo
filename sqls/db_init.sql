/*
    Choose serial over numeric
        Assumption id not overflow 2,147,483,647
    
*/
--WARN: be careful
drop schema if exists sources cascade;
drop schema if exists staging cascade;
drop schema if exists presentation cascade;

create schema sources;
create schema staging;
create schema presentation;

create table if not exists sources.user (
    id            serial unique primary key,
    firstname     text,
    -- middle_name text, --multi-lingual support. Since data is only english, so skipped.
    surname       text
);

create table if not exists sources.task (
    id            serial unique primary key,
    user_id       integer references sources.user(id),
    "name"        text,
    state         text
);

create table if not exists sources.activity (
    id            serial unique primary key,
    task_id       integer references sources.task(id), --Could assign FK after table creation for performance reason but here is convient.
    "date"        date,
    type          text
);

insert into sources.user values 
 (1, 'john', 'smith')
,(2, 'jane', 'doe')
,(3, 'micheal', 'bridge')
,(4, 'talia', 'doe')
,(5, 'mary jane', 'willamson')
,(6, 'dan', 'common')
,(7, 'obi', 'kenobi')
,(8, 'sarah', 'bourne')
;

insert into sources.task values
 (1, 1, 'clean 1 bedroom house', 'finalised')
,(2, 2, 'clean 4 bedroom house', 'active')
,(3, 3, 'clean 3 bedroom house', 'payment pending')
,(4, 3, 'mow lawn', 'finalised')
,(5, 4, 'remove garbage', 'finalised')
,(6, 5, 'clean 1 bedroom apartment', 'active')
,(7, 6, 'design web page', 'cancelled')
,(8, 3, 'costume makeup for party', 'suspend')
,(9, 7, 'post party cleanup', 'finalised')
,(10, 8, 'build tiki bar', 'finalised')
,(11, 2, 'clean 1 bedroom apartment', 'active')
,(12, 1, 'repair smashed tiger', 'active')
;

insert into sources.activity (task_id, date, type) values 
 (1, '2018-07-01', 'View')
,(1, '2018-07-01', 'Comment')
,(1, '2018-07-01', 'Comment')
,(1, '2018-07-01', 'Offer')

,(2, '2018-07-01', 'Offer')

,(3, '2018-07-01', 'View')
,(3, '2018-07-01', 'View')
,(3, '2018-07-01', 'View')

,(4, '2018-07-01', 'Offer')
,(4, '2018-07-01', 'Comment')
,(4, '2018-07-01', 'Offer')

,(5, '2018-07-01', 'Comment')
,(5, '2018-07-01', 'Comment')

,(6, '2018-07-01', 'View')
,(6, '2018-07-01', 'View')
,(6, '2018-07-01', 'Offer')

,(7, '2018-07-01', 'Offer')
,(7, '2018-07-01', 'Comment')
,(7, '2018-07-01', 'Comment')

,(8, '2018-07-01', 'Comment')
,(8, '2018-07-01', 'Comment')
,(8, '2018-07-01', 'Offer')
,(8, '2018-07-01', 'Offer')
,(8, '2018-07-01', 'Offer')
,(8, '2018-07-01', 'Comment')
,(8, '2018-07-01', 'Offer')

,(9, '2018-07-01', 'View')
,(9, '2018-07-01', 'Comment')
,(9, '2018-07-01', 'Offer')

,(10, '2018-07-01', 'View')
,(10, '2018-07-01', 'Offer')

,(11, '2018-07-01', 'View')

,(12, '2018-07-01', 'Comment')
,(12, '2018-07-01', 'Comment')
,(12, '2018-07-01', 'Offer')
;

/******************************************
set search_path to 'sources', public;

select * from sources.user;
select * from sources.task;
select * from sources.activity;
*******************************************/