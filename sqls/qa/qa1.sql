/* check source */
SELECT *
FROM sources.activity;

SELECT *
FROM sources.task;

SELECT *
FROM sources."user";


/* check presentation */
--no `current_records` on this table
select *
from presentation.snapshot_task;

select *
from presentation.tasks;

select *
from presentation.users;

