/*
  Snapshot Task Activity .
*/
create table if not exists presentation.snapshot_task (
    task_id                int,
    name                   text,

    view_count             int,
    offer_count            int,
    comment_count          int,

    snapshot_day           timestamp default now()::date,
    snapshot_time          timestamp default now()
);

/* only save rows in current batch to presentation layer
   'save-exact-once'
*/
insert into presentation.snapshot_task (
    task_id, 
    name,

    view_count,
    offer_count,
    comment_count
)
select
    ta.task_id,
    ta.name,

    ta.view_count,
    ta.offer_count,
    ta.comment_count
from staging.task_activity ta
order by 1,2;
