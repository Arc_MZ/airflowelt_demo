/*--------------------------------------------
    Task snapshots for `Task`
---------------------------------------------*/
create table if not exists presentation.tasks (
  --filled by task view
  id                      int,
  name                    text,
  status                  text,

  current_records         boolean   default true,
  snapshot_day            timestamp default now()::date,
  snapshot_time           timestamp default now()
);


--set existing records to non-current
update presentation.tasks
set current_records = false
where current_records = true;


--new records
insert into presentation.tasks (id, name, status)
select
  t.id,
  t.name,
  t.status
from staging.tasks t;


/*--------------------------------------------
    Take snapshots for `user`

    Issue: why do we need this ? if user base
           is huge..update sucks
---------------------------------------------*/
create table if not exists presentation.users (
  id                      int,      --bigint and bigserial too big for current scale.
  user_name               text,
  user_active_flag        text,

  current_records         boolean   default true,
  snapshot_day            timestamp default now()::date,
  snapshot_time           timestamp default now()
);


--Analyst can focus on present tasks and users.
update presentation.users
set current_records = false
where current_records = true;


insert into presentation.users (id, user_name, user_active_flag)
select
  u.id,
  u.user_name,
  u.user_active_flag
from staging.users u;
